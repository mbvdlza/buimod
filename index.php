<?php

/**
 * BuiMod
 * @author Marlon B v/d Linde
 * marlon@kbye.co.za
 * Copyright 2012
 */
error_reporting(E_ALL);



require_once('zone/Land.php');
require_once('zone/Point2D.php');
require_once('zone/ZoneBlock.php');

try { // ==== try to build the objects with the given points now ===
	$land = new Land(
					array(// a nice square 200x250 piece of land:
						new Point2D(0, 0),
						new Point2D(0, 250),
						new Point2D(200, 0),
						new Point2D(200, 250)
					)
	);

	$officeblock = new ZoneBlock(
					array(// a new zone that fits inside $land
						new Point2D(10, 10),
						new Point2D(10, 20),
						new Point2D(20, 10),
						new Point2D(20, 20)
					)
	);
} catch (InvalidArgumentException $e) {
	echo '<b>OBJECT BUILDING EXCEPTION: </b>' . $e->getMessage();
	exit;
}


$land->addResidentObject($officeblock);  // add this office block into the main land.









print "<pre>";
print_r($land);
print "</pre>";
?>













