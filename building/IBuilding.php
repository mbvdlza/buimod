<?php
/*
 * BuiMod
 * @author Marlon B v/d Linde
 * marlon@kbye.co.za
 * Copyright 2012
 */

/**
 * Template for building objects, mainly identification and venial toggling.
 *
 * @author Marlon B van der Linde
 */
interface IBuilding {

	/**
	 * Set the name of the building, for identification
	 * @param
	 */
	public function setName($name);

	/**
	 * Set the street name in which this building stands, for identification
	 * @param
	 */
	public function setStreet($streetName);

	/**
	 * Set the number of this building, as it stands in this street
	 * @param
	 */
	public function setNumber($streetNumber);

	/**
	 * The electric main switch for this building
	 * @param bool $bool
	 */
	public function electricalSupplyToggle($bool);

}

?>
