<?php

/*
 * BuiMod
 * @author Marlon B v/d Linde
 * marlon@kbye.co.za
 * Copyright 2012
 */

require_once('ICube.php');
require_once('IBuilding.php');
require_once('electric/IElectricSystem.php');
require_once('electric/ITransformer.php');

/*
 * Building class representing entire building.
 * Building is composed of FloorLevel objects and implements Cube
 *
 * @author Marlon B van der Linde
 */

class Building implements IBuilding, ICube, IElectricSystem {

	private $internalTransformer = null;
	protected $lengthX = 0;
	protected $heightY = 0;
	protected $depthZ = 0;
	private $dimensionUnit = 'm';
	private $powerUnit = 'w';
	private $volume;
	private $area;

	/**
	 * Array of devices/entities that consume power
	 * @var array
	 */
	private $powerConsumers = array();

	/**
	 * Reference to storage battery object
	 * @var StorageBattery
	 */
	private $battery = null;

	/**
	 * Building construction, no pun intended.
	 * @param float $x length of building
	 * @param float $y height of building
	 * @param float $z depth of building
	 */
	public function __construct($x, $y, $z) {
		$this->defineLengthX($x);
		$this->defineHeightY($y);
		$this->defineDepthZ($z);
	}

	/**
	 * Set the X(width) length of the building, and recalculate the volume and ground floor area
	 * @param int $val
	 */
	public function defineLengthX($val) {
		$this->lengthX = (float) $val;
		$this->calculateVolume();
		$this->calculateArea();
	}

	/**
	 * Set the Y(height) length of the building, and recalculate the volume
	 * @param int $val
	 */
	public function defineHeightY($val) {
		$this->heightY = (float) $val;
		$this->calculateVolume();
	}

	/**
	 * Set the Z(depth) length of the building, and recalculate the volume and ground floor area
	 * @param int $val
	 */
	public function defineDepthZ($val) {
		$this->depthZ = (float) $val;
		$this->calculateVolume();
		$this->calculateArea();
	}

	/**
	 * Set the measurement units used for w/h/l and power
	 * @param string $dimensionUnit unit of measurement of dimension
	 * @param string $powerUnit unit of measurement of power
	 */
	public function setUnits($dimensionUnit = 'm', $powerUnit = 'w') {
		$mUnitsAllowed = array('m', 'ft');
		if (in_array($dimensionUnit, $mUnitsAllowed)) {
			$this->dimensionUnit = $unit;
		} else {
			$this->dimensionUnit = 'm';
		}

		$pUnitsAllowed = array('w');
		if (in_array($powerUnit, $mUnitsAllowed)) {
			$this->powerUnit = $unit;
		} else {
			$this->powerUnit = 'w';
		}
	}

	public function getUnit() {
		return $this->unit;
	}

	/**
	 * Calulcate the volume of the inside of this building.
	 * Prefer not to call publically.
	 */
	public function calculateVolume() {
		$this->volume = (float) $this->lengthX * $this->heightY * $this->depthZ;
	}

	/**
	 * Calulcate the floor of the bottom ground floor of this building.
	 * In other words, area used on city ground.
	 * Prefer not to call publically.
	 */
	public function calculateArea() {
		$this->area = (float) $this->lengthX * $this->heightY;
	}

	public function setName($name) {

	}

	public function setStreet($streetName) {

	}

	public function setNumber($streetNumber) {

	}

	public function electricalSupplyToggle($bool) {

	}

	/**
	 * Set this building's transformer to the provided ITransformer
	 * @todo REFERENCE building transformer rather than keeping copy
	 * @param ITransformer $internalTransformer
	 */
	public function setInternalTransformer(ITransformer $internalTransformer) {
		$this->internalTransformer = &$internalTransformer;
	}

	/**
	 * Returns a reference to this buildings internal transformer, for connection to upstream transformer.
	 */
	public function getInternalTransformer() {
		return $this->internalTransformer;
	}

	/**
	 * Adds a device/anything that consumes power on the local object transformer.
	 * @param int $id ID of the consumer (@todo lookup table)
	 * @param int $usage the usage given predefined units
	 */
	public function addPowerConsumer($id, $usage) {
		// @todo validate call
		$this->powerConsumers[$id] = $usage;
		$this->internalTransformer->addLoad($usage);
	}

	public function removePowerConsumer($id) {
		// @todo validate call and check issets
		$load = $this->powerConsumers[$id];
		unset($this->powerConsumers[$id]);
		$this->internalTransformer->removeLoad($load);
	}

	private function output($msg) {
		print '<br /><b>Class</b>::' . __CLASS__ . ':: <i>' . $msg . '</i>';
	}

}

?>
