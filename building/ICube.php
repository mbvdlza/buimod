<?php
/**
 * Template for 3D objects, with height, width and depth dimensions.
 * As used for BuiMod's rooms, levels, etc
 *
 * BuiMod
 * @author Marlon B v/d Linde
 * marlon@kbye.co.za
 * Copyright 2012
 */
interface ICube {
    public function defineLengthX($val);
    function defineHeightY($val);
    function defineDepthZ($val);
    function calculateVolume();
    function calculateArea();
    function getUnit();
}
?>
