
Copyright 2012, M van der Linde <marlon@kbye.co.za>
---
Hang tight.
Proper licensing under consideration. Opensource highly probable.
---
Until further notice, all source, images, and content created by Marlon van der Linde, for BuiMod, remains closed and unsuitable for any form of usage or distribution or modification.
All rights reserved, nothing allowed at all.
---
marlon@kbye.co.za
http://git.kbye.co.za
