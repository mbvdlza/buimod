<?php

/**
 * Buimod
 * @author Marlon B v/d Linde
 * marlon@kbye.co.za
 * Copyright 2012
 */
require_once('/zone/Point2D.php');

/**
 * Class CollidableObject
 * Contains static or otherwise functions to do one-off calculations and work regarding
 * collidable objects (collision detection, perimeter calculations etc).
 */
abstract class CollidableObject {

	private function __construct() {

	}

	private function __destruct() {

	}

	/**
	 * Checks if the new candidate resident object fits (and do not overlap the borders of) the hosting zone object.
	 * Takes two arrays of IPoints.
	 * @param array $host Array of IPoints
	 * @param array $candidate Array of IPoints
	 * @return boolean
	 */
	static protected function perimeterCheck(array &$host, array $candidate) {

		// @todo FIND a way to do proper perimeter checks and overlap detection

		print "<br />Host: <pre>";
		print_r($host);
		print "</pre><br />Candidate<pre>";
		print_r($candidate);
		print "</pre>";

		print $host[0]->getLongitude();

		return true;
	}

}

?>
