<?php
/**
 * Buimod
 * Interface IZone
 * Land class implementation template
 *
 * @author Marlon B v/d Linde
 * marlon@kbye.co.za
 * Copyright 2012
 */
interface IZone {
	public function getBorderPoints();
}

?>
