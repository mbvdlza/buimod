<?php

/**
 * BuiMod
 * @author Marlon B v/d Linde
 * marlon@kbye.co.za
 * Copyright 2012
 */
require_once('Ipoint.php');

/**
 * Class Point
 * A 2D land address based on top-down-view type X/Y coordinate system
 *
 */
class Point2D implements IPoint {

	private $lon;
	private $lat;

	/**
	 * Create a two dimensional point with the longitude and latitude values provided.
	 * @param int $lon
	 * @param int $lat
	 */
	public function __construct($lon, $lat) {
		$this->setCoords($lon, $lat);
	}

	/**
	 * Private setter called at construction time, to set coordinates, and limit mutability of a point.
	 * Fairly redundant at this point, but reserves space for create-time control later.
	 */
	private function setCoords($lon, $lat) {
		$this->lon = (int) $lon;
		$this->lat = (int) $lat;
	}

	/**
	 * Return the longitude of this point
	 * @return int
	 */
	public function getLongitude() {
		return $this->lon;
	}

	/**
	 * Return the latitude of this point
	 * @return type
	 */
	public function getLatitude() {
		return $this->lat;
	}

}

?>
