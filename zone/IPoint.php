<?php
/*
 * 2 dimensional point interface
 */

/**
 * Interface IPoint
 *
 */
interface IPoint {
	public function getLongitude();
	public function getLatitude();
}

?>
