<?php

/**
 * Buimod
 * @author Marlon B v/d Linde
 * marlon@kbye.co.za
 * Copyright 2012
 */
require_once('IZone.php');
require_once('IPoint.php');
require_once("/extra/CollidableObject.php");

/**
 * Class Land, the largest logical part of Buimod
 * 2 Dimension land zone class
 */
class Land extends CollidableObject implements IZone {

	private $borderPoints;
	private $subzones;

	public function __construct(array $array_of_points) {
		$this->borderPoints = array();
		$this->subzones = array();
		$this->setBorderPoints($array_of_points);
	}

	public function __destruct() {

	}

	/**
	 * Return an array containing the points of the complete border of this Land
	 */
	public function getBorderPoints() {
		return $this->borderPoints;
	}

	/**
	 * Set the border points to form a closed zone of Land.
	 * An array of IPoint's must be provided or an InvalidArgumentException will be thrown.
	 * @param array $array_of_points
	 */
	private function setBorderPoints(array $array_of_points) {
		foreach ($array_of_points as $ipoint) {
			if (!$ipoint instanceof IPoint) {
				throw new InvalidArgumentException('Class: ' . __CLASS__
						. ' - Points array values must all be of type IPoint');
			}
			$this->borderPoints[] = $ipoint;
		}
	}

	/**
	 * Stores a new subzone on this Land. Only certain types/instance's can live on a land. Anything that is not
	 * fit to live on the Land, will throw an exception
	 * @param IZone $zoneblock
	 */
	public function addResidentObject(IZone $subzone) {
		/**
		 * @todo: check if this block collides with an existing block and throw an exception
		 * @todo: check the instanceof/type of $subzone to further control the subzone structures(water/blocks etc)
		 * @todo: check the perimeters. Must fit inside this land
		 */
		if (!parent::perimeterCheck($this->getBorderPoints(), $subzone->getBorderPoints())) {
			$this->subzones[] = &$subzone;
		}
	}

}

?>
