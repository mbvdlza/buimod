<?php

/**
 * Buimod
 * @author Marlon B v/d Linde
 * marlon@kbye.co.za
 * Copyright 2012
 */
require_once('IZone.php');
require_once('/extra/CollidableObject.php');

/**
 * Class ZoneBlock
 * This is a smaller "block" inside a land. Blocks live on Land, Buildings live on Blocks
 */
class ZoneBlock extends CollidableObject implements IZone {

	private $borderPoints;
	private $buildings;

	public function __construct(array $array_of_points) {
		$this->borderPoints = array();
		$this->setBorderPoints($array_of_points);
	}

	public function __destruct() {

	}

	/**
	 * Return an array containing the points of the complete border of this zone block
	 */
	public function getBorderPoints() {
		return $this->borderPoints;
	}

	/**
	 * Set the border points to form a closed zone block.
	 * An array of IPoint's must be provided or an InvalidArgumentException will be thrown.
	 * @param array $array_of_points
	 */
	private function setBorderPoints(array $array_of_points) {
		foreach ($array_of_points as $ipoint) {
			if (!$ipoint instanceof IPoint) {
				throw new InvalidArgumentException('Class: ' . __CLASS__
						. ' - Points array values must all be of type IPoint');
			}
			$this->borderPoints[] = $ipoint;
		}
	}

	/**
	 * Stores a new subzone on this block. Only IBuildings can live on a zone block.
	 * @param IBuilding $building
	 */
	public function addResidentObject(IBuilding $building) {
		/**
		 * @todo: check if this building collides with an existing block and throw an exception
		 */
	}

}

?>
