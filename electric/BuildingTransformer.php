<?php
/*
 * BuiMod
 * @author Marlon B v/d Linde
 * marlon@kbye.co.za
 * Copyright 2012
 */

require_once('ITransformer.php');

/**
 * Class buildingTransformer
 * This is the internal local transformer for this Building.
 * It is supplied from an external transformer and of type Transformer
 * @author Marlon B van der Linde
 */
class BuildingTransformer implements ITransformer {

    private $maxCapacity;
    private $usage;

    public function __construct() {
    }

	/**
	 * Returns thos transformer's power usage
	 * @return int
	 */
    public function getUsage() {
        return $this->usage;
    }

	/**
	 * Adds load in power units, to this transformer
	 * @param int $loadUnits
	 */
	public function addLoad($loadUnits) {
		$this->usage += $loadUnits;
	}

	/**
	 * Removes load from this transformer
	 * @param type $loadUnits
	 */
	public function removeLoad($loadUnits) {
		$this->usage -= $loadUnits;
	}


}

?>