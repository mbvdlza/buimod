<?php
/*
 * BuiMod
 * @author Marlon B v/d Linde
 * marlon@kbye.co.za
 * Copyright 2012
 */

require_once('ITransformer.php');

/**
 * Singleton Class StreetTransformer
 * This is the external public transformer for the block or street.
 * There can only be one external transformer.(for now)
 * @author Marlon B van der Linde
 */
class StreetTransformer implements ITransformer {

    public $maxCapacity = 0;
    public $usage = 0;
	/**
	 * array of transformers downstream/using power from this one
	 * @var array
	 */
	private $consumers = array();

    private static $instance;

	/**
	 * Private constructor, prevent instantiation
	 */
    private function __construct() {
    }

    /**
     * There shall be only one building transformer in this building
     * @return type
     */
    public static function singleton() {
        if (!isset(self::$instance)) {
            $spawn = __CLASS__;
            self::$instance = new $spawn();
        }
        return self::$instance;
    }

	/**
	 * connects a downstream transformer as a consumer on this one, usually building transformers
	 * @todo keep reference of consumer ITransformer instead of object copy
	 * @param ITransformer $consumer
	 */
	public function connect(ITransformer $consumer) {
		$this->consumers[] = $consumer;
		$this->calculateUsage();
	}

	/**
	 * Calculate the usage by checking the total consumers on each downstream transformer
	 */
    public function calculateUsage() {
        $this->usage = 0;
		foreach ($this->consumers as $consumer) {
			$this->usage += $consumer->getUsage();
		}
    }

	/**
	 * Getter for the usage on this transformer
	 * @return int
	 */
	 public function getUsage() {
		$this->calculateUsage();
		return $this->usage;
	 }

	/**
	 * @todo : Stubbed. We can, but don't want to connect to an upstream transformer from Street yet
	 */
	public function connectToUpstreamTransformer(ITransformer $upstreamTrafo) {
		// stubbed!!!!
	}

	/**
	 * Adds load in power units, to this transformer
	 * @param int $loadUnits
	 */
	public function addLoad($loadUnits) {
		$this->usage += $loadUnits;
		$this->calculateUsage();
	}

		/**
	 * Removes load from this transformer
	 * @param type $loadUnits
	 */
	public function removeLoad($loadUnits) {
		$this->usage -= $loadUnits;
		$this->calculateUsage();
	}
}

?>
