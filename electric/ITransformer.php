<?php
/*
 * BuiMod
 * @author Marlon B v/d Linde
 * marlon@kbye.co.za
 * Copyright 2012
 */

/**
 * Interface Transformer
 * General Transformer template, all transformers are based on this interface.
 *
 */
interface ITransformer {

	/**
	 * Returns usage on this transformer
	 * @return int
	 */
	public function getUsage();

	/**
	 * Connects to the upstream street transformer; registers itself as a consumer to it.
	 * @param ITransformer $upstreamTrafo
	 */
	//public function connectToUpstreamTransformer(ITransformer $upstreamTrafo);

	/**
	 * Adds load in power units, to this transformer
	 * @param int $loadUnits
	 */
	public function addLoad($loadUnits);

	/**
	 * Removes load in power units, from this transformer
	 * @param int $loadUnits
	 */
	public function removeLoad($loadUnits);

}


?>
