<?php
/*
 * BuiMod
 * @author Marlon B v/d Linde
 * marlon@kbye.co.za
 * Copyright 2012
 */

/**
 * Interface ElectricSystem
 * Templating for electrical supply management
 * @author Marlon B van der Linde
 */
interface IElectricSystem {

	/**
	 * Set this building's transformer to the one given, and connects it to the upstream transformer
	 * @param ITransformer $internalTransformer
	 * @param ITransformer $streetTransformer
	 */
	public function setInternalTransformer(ITransformer $internalTransformer);

	/**
	 * Get this building's transformer
	 * @return ITransformer
	 */
	public function getInternalTransformer();

	/**
	 * Adds a device/anything that consumes power on the local transformer.
	 * @param int $id ID of the consumer ($todo lookup table)
	 * @param int $usage the usage given predefined units
	 */
	public function addPowerConsumer($id, $usage);

	/**
	 * Removes a previously added entity that consumes power on the local transformer.
	 * @param int $id ID of the consumer ($todo lookup table)
	 */
	public function removePowerConsumer($id);

}

?>
