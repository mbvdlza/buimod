<?php

/*
 * BuiMod
 * @author Marlon B v/d Linde
 * marlon@kbye.co.za
 * Copyright 2012
 */

/**
 * Debugging And Development Tools
 */
class Debug {

	private $tracking;
	private $silent;

	public function __construct($silent = 0) {
		$this->tracking = array();
		$this->tracking['start_time_main'] = microtime();
		$this->silent = $silent;
	}

	public function __destruct() {
		$this->tracking['stop_time_main'] = microtime();

		if (!$this->silent) {
			print "<h4>Debug</h4><pre>";
			print_r($this->tracking);
		}
	}

	public function setMemAtLabel($label) {
		$this->tracking["mem_at_$label"] = memory_get_peak_usage();
	}

	public function timeMemAtLabel($label) {
		$this->tracking["time_at_$label"] = microtime();
	}

}

?>
