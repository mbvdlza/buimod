<?php
/**
 * Development : Testing/Documenting Building/Transformer assembly
 */
require_once('Building/Building.php');
require_once('Debug.php');
require_once('electric/BuildingTransformer.php');
require_once('electric/StreetTransformer.php');

$debug = new Debug();


// ========= BUILDING ASSEMBLY 101 =================//
$xOffices = new Building(20, 40, 20);		   // building construction, irony!
$xOffices->setInternalTransformer(new BuildingTransformer());	 // assign a building transformer
StreetTransformer::singleton()->connect($xOffices->getInternalTransformer()); // connect this bldng trafo to street
$xOffices->addPowerConsumer(1, 100);		   // add something that uses 100watts
$xOffices->addPowerConsumer(2, 150);		   // add something that uses 150watts
$xOffices->addPowerConsumer(3, 50);			// add something that uses 50watts

$debug->timeMemAtLabel('xOffices');

// ============================================//


$zOffices = new Building(20, 40, 20); // building construction, irony!
$zOffices->setInternalTransformer(new BuildingTransformer());
StreetTransformer::singleton()->connect($zOffices->getInternalTransformer());
$zOffices->addPowerConsumer(1, 30);
$zOffices->addPowerConsumer(2, 60);
$zOffices->addPowerConsumer(3, 90);

$debug->timeMemAtLabel('zOffices');

$fOffices = new Building(5, 10, 10); // building construction, irony!
$fOffices->setInternalTransformer(new BuildingTransformer());
StreetTransformer::singleton()->connect($fOffices->getInternalTransformer());
$fOffices->addPowerConsumer(1, 7620);
$fOffices->addPowerConsumer(2, 5420);
$fOffices->addPowerConsumer(3, 12320);

$debug->timeMemAtLabel('fOffices');

// calculate the latest usage totals on the street transformer
StreetTransformer::singleton()->calculateUsage();
?>

<!-- Ugliest debug html in the world -->
<center><table border='1px' width='80%' ><tr><td>
<?php var_dump($xOffices); ?>
			</td><td>
<?php var_dump($zOffices); ?>
			</td><td>
<?php var_dump($fOffices); ?>
			</td></tr><tr><td colspan=3>
<?php var_dump(StreetTransformer::singleton()); ?>
			</td></tr></table></center>
